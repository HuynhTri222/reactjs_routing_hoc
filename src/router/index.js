import Home from "../containers/home/HomePage";
import About from "../containers/home/AboutPage";
import ListMovie from "../containers/home/ListMoviePage";
import HOCPage from "../containers/home/HOCPage";

//Quan ly cac Routes trong app.js
//3 cai thuoc tinh hien gio la co dinh, khi dinh nghia doi tuong route nao cung co
const routesHome = [
    {
        exact: true,
        path: "/",
        component: Home,
    },
    {
        exact: false,
        path: "/about",
        component: About,
    },
    {
        exact: false,
        path: "/list-movie",
        component: ListMovie,
    },
    {
        exact: false,
        path: "/HOCPage",
        component: HOCPage,
    }
];

export {routesHome};
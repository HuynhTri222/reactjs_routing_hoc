//Chua cac phan HTML chung cho cac component
import React from "react";

// Day la HOC
// Truyen vao tham so la 1 component
function WithModal(Component) {
    //vi la HOC nen
    return function(){
        return (
            <div>
              <div>
                <div>
                  {/* Button trigger modal */}
                  <button
                    type="button"
                    className="btn btn-primary btn-lg"
                    data-toggle="modal"
                    data-target="#modelId"
                  >
                    Launch
                  </button>
                  {/* Modal */}
                  <div
                    className="modal fade"
                    id="modelId"
                    tabIndex={-1}
                    role="dialog"
                    aria-labelledby="modelTitleId"
                    aria-hidden="true"
                  >
                    <div className="modal-dialog" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Modal title</h5>
                          <button
                            type="button"
                            className="close"
                            data-dismiss="modal"
                            aria-label="Close"
                          >
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        {/* Goi tham so component ra */}
                        <div className="modal-body">
                            <Component/>
                        </div>
                        <div className="modal-footer">
                          <button
                            type="button"
                            className="btn btn-secondary"
                            data-dismiss="modal"
                          >
                            Close
                          </button>
                          <button type="button" className="btn btn-primary">
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
    }
 
}

export default WithModal;

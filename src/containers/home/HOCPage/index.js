import React, { Component } from 'react';
import NguoiDung from './nguoiDung';
import SanPham from './sanPham';
import WithModal from './WithModal';

const FormModal = WithModal(SanPham);

//HOC Higher-Order Components dua vao closure
class HOCPage extends Component {
    render() {
        return (
            <div>
                {/* <NguoiDung/> */}
                {/* <SanPham/> */}
                <FormModal/>
            </div>
        );
    }
}

export default HOCPage;
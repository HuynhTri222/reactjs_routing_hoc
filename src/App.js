import React from 'react';
import './App.css';
// Cai nay dung de dieu huong cac component theo duong dan
import { BrowserRouter, Route, Switch } from "react-router-dom";


// import Home from './containers/home/HomePage';
// import About from './containers/home/AboutPage';
// import ListMovie from './containers/home/ListMoviePage';
import PageNotFound from './containers/PageNotFound';
import Navbar from './components/navbar';
import { routesHome } from './router/index.js';

function App() {
  const renderHomeLayout = (routes) => {
    if(routes && routes.length > 0) {
      //Tai sao dung map ma khong dung foreach
      // Vi map no return ve 1 mang moi sau khi chay xong 
      return routes.map((item, index) => {
        // Can than Route va Router - DE LON
        return <Route
                  key={index} 
                  exact={item.exact} 
                  path={item.path} 
                  component={item.component} 
                />;
                
      })
    }
  };

  return (
    <BrowserRouter>
    <Navbar></Navbar>
{/* Switch cai nay nhu switch case, tach rieng tung route */}
      <Switch>
        {renderHomeLayout(routesHome)}
        {/* Trang chu */}
        {/* Cai exact them de cho no hieu trang nay phai dung dung cai duong dan nay */}
        {/* <Route exact path="/" component={Home} /> */}
        {/* Trang About */}
        {/* <Route path="/about" component={About} /> */}
        {/* Trang List Movie */}
        {/* <Route path="/list-movie" component={ListMovie} /> */}
        {/* Page Not Found - cac duong link khong tim thay - de no cuoi cung trong cac bo route*/}
        <Route path="" component={PageNotFound} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;

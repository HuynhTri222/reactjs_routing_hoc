import React, { Component } from 'react';
// Vi the a la nguyen nhan lam cho trang bi load lai khi click vao link nen minh chuyen sang dung cai nay
// import { Link } from "react-router-dom";
// Muon active cai link thi dung cai nay khong dung Link
import { NavLink } from "react-router-dom";
class Navbar extends Component {
    render() {
        return (          
<nav className="navbar navbar-expand-md bg-dark navbar-dark">
    <a className="navbar-brand" href="#">Navbar</a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span className="navbar-toggler-icon" />
    </button>
    <div className="collapse navbar-collapse" id="collapsibleNavbar">
      <ul className="navbar-nav">
        <li className="nav-item">
            {/* Link thi di voi attribute to */}
            {/* khi chuyen qua cac trang khac thi trang home van bi dinh vi dau / trong link, khac phuc dieu nay thi nhu trong app.js dung exact */}
          <NavLink exact activeClassName="active" className="nav-link" to="/">Home</NavLink>
        </li>
        <li className="nav-item">
          <NavLink activeClassName="active" className="nav-link" to="/about">About</NavLink>
        </li>
        <li className="nav-item">
          <NavLink activeClassName="active" className="nav-link" to="/list-movie">List Movie</NavLink>
        </li>
        <li className="nav-item">
          <NavLink activeClassName="active" className="nav-link" to="/HOCPage">HOC Page</NavLink>
        </li>    
      </ul>
    </div>  
  </nav>
);

}
}
export default Navbar;